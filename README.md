
An Android and iOS application that proactively budgets for you, by utilizing machine learning techniques and geolocation to create and maintain a budget specialized for you.
